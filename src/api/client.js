import request from '@/utils/request'

export function getClientList(params) {
  return request({
    url: '/system/client/list',
    method: 'get',
    params: params
  })
}
export function saveClient(clientId, clientSecret, resourceIds, scope, accessTokenValidity, refreshTokenValidity) {
  return request({
    url: '/system/client/add',
    method: 'put',
    params: {
      clientId: clientId,
      clientSecret: clientSecret,
      resourceIds: resourceIds,
      scope: scope,
      accessTokenValidity: accessTokenValidity,
      refreshTokenValidity: refreshTokenValidity
    }
  })
}
export function editClient(id, clientSecret) {
  return request({
    url: '/system/client/' + id,
    method: 'post',
    params: {
      clientSecret: clientSecret
    }
  })
}
export function getClient(id) {
  return request({
    url: '/system/client/' + id,
    method: 'get',
    params: {
    }
  })
}

