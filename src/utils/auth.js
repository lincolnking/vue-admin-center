import Cookies from 'js-cookie'

const TokenKey = 'Admin-Token'
const REFRESH_TOKEN = 'REFRESH_TOKEN'

export function getToken() {
  var cookie = getCookie(TokenKey)
  // console.log(cookie)
  return cookie
}
export function getRefreshToken() {
  return getCookie(REFRESH_TOKEN)
}

export function setToken(token) {
  return setCookie(TokenKey, token, 900)
}

export function setRefreshToken(token) {
  return setCookie(REFRESH_TOKEN, token, 3600)
}

export function setCookie(c_name, value, expiredays) {
  var exdate = new Date()
  exdate = new Date(exdate.getTime() + expiredays * 1000)
  document.cookie = c_name + '=' + escape(value) + ((expiredays == null) ? '' : ';expires=' + exdate.toGMTString())
}

export function getCookie(name) {
  var strcookie = document.cookie
  var arrcookie = strcookie.split('; ')
  for (var i = 0; i < arrcookie.length; i++) {
    var arr = arrcookie[i].split('=')
    if (arr[0] === name) {
      return arr[1]
    }
  }
  return ''
}

export function removeToken() {
  return Cookies.remove(TokenKey)
}
