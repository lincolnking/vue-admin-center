import request from '@/utils/request'

export function getRoleListAll() {
  var res = request({
    url: '/system/role/list',
    method: 'get',
    params: {
      pageNo: 0,
      pageSize: 9999
    }
  })
  return res
}

export function getRoleList(params) {
  return request({
    url: '/system/role/list',
    method: 'get',
    params: params
  })
}
export function saveRole(name, permissions) {
  var pers = ''
  for (var per in permissions) {
    pers += (pers === '' ? '' : ',') + permissions[per]
  }
  return request({
    url: '/system/role/add',
    method: 'put',
    params: {
      name: name,
      permissions: pers
    }
  })
}
export function editRole(id, name, permissions) {
  var pers = ''
  for (var per in permissions) {
    pers += (pers === '' ? '' : ',') + permissions[per]
  }
  return request({
    url: '/system/role/' + id,
    method: 'post',
    params: {
      name: name,
      permissions: pers
    }
  })
}
export function getRole(id) {
  return request({
    url: '/system/role/' + id,
    method: 'get',
    params: {
    }
  })
}
export function getAllpermissions(id) {
  return request({
    url: '/system/permission/list',
    method: 'get',
    params: {
    }
  })
}
