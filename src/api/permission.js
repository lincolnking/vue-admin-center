import request from '@/utils/request'

export function getPermissionListAll() {
  return request({
    url: '/system/permission/list',
    method: 'get',
    params: {
      pageNo: 0,
      pageSize: 9999
    }
  })
}
export function getPermissionList(params) {
  return request({
    url: '/system/permission/list',
    method: 'get',
    params: params
  })
}
export function savePermission(permissions) {
  return request({
    url: '/system/permission/add',
    method: 'put',
    params: {
      permissions: permissions
    }
  })
}
export function editPermission(id, name) {
  return request({
    url: '/system/permission/' + id,
    method: 'post',
    params: {
      name: name
    }
  })
}
export function getPermission(id) {
  return request({
    url: '/system/permission/' + id,
    method: 'get',
    params: {
    }
  })
}
