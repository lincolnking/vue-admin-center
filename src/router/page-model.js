export const modelMap = {
  '/sys/user': {
    model: {
      apiUrlHeader: '/system/user',
      router: '/sys/user',
      attrs: [
        {
          name: 'userName',
          describe: '用户名',
          type: 'string',
          show: true,
          need: true
        },
        {
          name: 'password',
          describe: '密码',
          type: 'string',
          show: false,
          need: true
        },
        {
          name: 'roleId',
          entity: 'role',
          valueName: 'id',
          dataName: 'name', // json中取出name值作为显示
          describe: '等级',
          type: 'fetch-enum',
          fetchEnumUrl: '/system/role/list',
          fetchEnumMethod: 'get',
          fetchEnumParams: {},
          fetchEnumListName: 'data.content', // list在请求体的位置
          fetchEnumName: 'name', // 请求回来的list显示的名称
          fetchEnumValue: 'id', // 请求回来的list显示的名称
          show: true,
          need: true
        }
      ]
    }
  },
  '/sys/client': {
    model: {
      apiUrlHeader: '/system/client',
      router: '/sys/client',
      attrs: [
        {
          name: 'clientId',
          describe: '客户端id',
          type: 'string',
          show: true,
          need: true
        },
        {
          name: 'resourceIds',
          describe: '支持的资源服务器',
          type: 'string',
          show: true,
          need: true
        },
        {
          name: 'clientSecret',
          describe: '客户端密码',
          type: 'string',
          show: false,
          need: true
        },
        {
          name: 'scope',
          describe: '支持的用户等级',
          type: 'string',
          show: true,
          need: true
        },
        {
          name: 'accessTokenValidity',
          describe: 'token时长',
          type: 'int',
          default: 900,
          show: true,
          need: true
        },
        {
          name: 'refreshTokenValidity',
          describe: 'refresh_token时长',
          type: 'int',
          default: 3600,
          show: true,
          need: true
        }
      ]
    }
  },
  '/sys/permission': {
    model: {
      apiUrlHeader: '/system/permission',
      router: '/sys/permission',
      attrs: [
        {
          name: 'name',
          describe: '权限名',
          type: 'string',
          show: true,
          need: true
        }
      ]
    }
  }
}
export const queryMap = {
  '/sys/user': {
    query: {}
  },
  '/sys/client': {
    query: {}
  },
  '/sys/permission': {
    query: {}
  }
}
