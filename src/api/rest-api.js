import request from '@/utils/request'

export function getListAll(urlHeader) {
  return request({
    url: urlHeader + '/list',
    method: 'get',
    params: {
      pageNo: 0,
      pageSize: 9999
    }
  })
}
export function getList(urlHeader, params) {
  return request({
    url: urlHeader + '/list',
    method: 'get',
    params: params
  })
}
export function save(urlHeader, entity) {
  return request({
    url: urlHeader,
    method: 'put',
    data: entity
  })
}
export function edit(urlHeader, id, entity) {
  return request({
    url: urlHeader + '/' + id,
    method: 'post',
    data: entity
  })
}
export function get(urlHeader, id) {
  return request({
    url: urlHeader + '/' + id,
    method: 'get',
    params: {
    }
  })
}

