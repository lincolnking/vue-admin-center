import request from '@/utils/request'

export function getUserList(params) {
  return request({
    url: '/system/user/list',
    method: 'get',
    params: params
  })
}
export function saveUser(username, password, roleId) {
  return request({
    url: '/system/user/add',
    method: 'put',
    params: {
      userName: username,
      password: password,
      roleId: roleId
    }
  })
}
export function editUser(id, password, roleId) {
  return request({
    url: '/system/user/' + id,
    method: 'post',
    params: {
      password: password,
      roleId: roleId
    }
  })
}
export function getUser(id) {
  return request({
    url: '/system/user/' + id,
    method: 'get',
    params: {
    }
  })
}
export function getUserpermissions(id) {
  return request({
    url: '/system/permission/user/' + id,
    method: 'get',
    params: {
    }
  })
}
export function postUserpermissions(id, permissions) {
  return request({
    url: '/system/permission/user/' + id,
    method: 'post',
    params: {
      permissionIds: permissions
    }
  })
}

