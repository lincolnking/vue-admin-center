import { login, logout, getInfo } from '@/api/login'
import { getToken, setToken, removeToken, setRefreshToken } from '@/utils/auth'

const user = {
  state: {
    token: getToken(),
    name: '',
    refreshToken: ''
  },

  mutations: {
    SET_TOKEN: (state, token) => {
      state.token = token
    },
    SET_REFRESH_TOKEN: (state, refresh_token) => {
      state.refreshToken = refresh_token
    },
    SET_NAME: (state, name) => {
      state.name = name
    }
  },

  actions: {
    SetToken({ commit }, token) {
      commit('SET_TOKEN', token)
    },
    // 登录
    Login({ commit }, userInfo) {
      const username = userInfo.username.trim()
      return new Promise((resolve, reject) => {
        login(username, userInfo.password).then(response => {
          const data = response.data
          setToken(data.DefaultOAuth2AccessToken.access_token)
          setRefreshToken(data.DefaultOAuth2AccessToken.refresh_token)
          commit('SET_TOKEN', data.DefaultOAuth2AccessToken.access_token)
          commit('SET_REFRESH_TOKEN', data.DefaultOAuth2AccessToken.refresh_token)
          resolve()
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 获取用户信息
    GetInfo({ commit, state }) {
      return new Promise((resolve, reject) => {
        getInfo(state.token).then(response => {
          const data = response.data
          commit('SET_NAME', data.name)
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 登出
    LogOut({ commit, state }) {
      return new Promise((resolve, reject) => {
        logout(state.token).then(() => {
          commit('SET_TOKEN', '')
          commit('SET_REFRESH_TOKEN', '')
          removeToken()
          resolve()
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 前端 登出
    FedLogOut({ commit }) {
      return new Promise(resolve => {
        commit('SET_TOKEN', '')
        commit('SET_REFRESH_TOKEN', '')
        removeToken()
        resolve()
      })
    }
  }
}

export default user
