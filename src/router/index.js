import Vue from 'vue'
import Router from 'vue-router'

// in development-env not use lazy-loading, because lazy-loading too many pages will cause webpack hot update too slow. so only in production use lazy-loading;
// detail: https://panjiachen.github.io/vue-element-admin-site/#/lazy-loading

Vue.use(Router)

/* Layout */
import Layout from '../views/layout/Layout'

/**
* hidden: true                   if `hidden:true` will not show in the sidebar(default is false)
* alwaysShow: true               if set true, will always show the root menu, whatever its child routes length
*                                if not set alwaysShow, only more than one route under the children
*                                it will becomes nested mode, otherwise not show the root menu
* redirect: noredirect           if `redirect:noredirect` will no redirct in the breadcrumb
* name:'router-name'             the name is used by <keep-alive> (must set!!!)
* meta : {
    title: 'title'               the name show in submenu and breadcrumb (recommend set)
    icon: 'svg-name'             the icon show in the sidebar,
  }
**/
export const constantRouterMap = [
  { path: '/sys/login', component: () => import('@/views/login/index'), hidden: true },
  { path: '/404', component: () => import('@/views/404'), hidden: true },

  {
    path: '/sys/',
    component: Layout,
    redirect: '/sys/user/list',
    name: '用户列表',
    hidden: true
    // children: [{
    //   path: 'dashboard',
    //   component: () => import('@/views/dashboard/index')
    // }]
  },

  {
    path: '/sys/example',
    component: Layout,
    redirect: '/sys/example/table',
    name: 'Example',
    meta: { title: 'Example', icon: 'example' },
    children: [
      {
        path: 'table',
        name: 'Table',
        component: () => import('@/views/table/index'),
        meta: { title: 'Table', icon: 'table' }
      },
      {
        path: 'tree',
        name: 'Tree',
        component: () => import('@/views/tree/index'),
        meta: { title: 'Tree', icon: 'tree' }
      }
    ]
  },

  {
    path: '/sys/form',
    component: Layout,
    children: [
      {
        path: 'index',
        name: 'Form',
        component: () => import('@/views/form/index'),
        meta: { title: 'Form', icon: 'form' }
      }
    ]
  },

  {
    path: '/sys/user',
    component: Layout,
    redirect: '/sys/user/list',
    name: '用户管理',
    meta: { title: '用户管理', icon: 'example' },
    children: [
      {
        path: 'list',
        name: '用户列表',
        component: () => import('@/views/common/list'),
        meta: { title: '用户列表', icon: 'table' }
      },
      {
        path: 'add',
        name: '新增用户',
        component: () => import('@/views/common/add'),
        meta: { title: '新增用户', icon: 'tree' }
      },
      {
        path: 'edit',
        name: '编辑用户',
        component: () => import('@/views/common/edit'),
        meta: { title: '编辑用户', icon: 'tree' },
        hidden: true
      }
    ]
  },

  {
    path: '/sys/client',
    component: Layout,
    redirect: '/sys/client/list',
    name: '客户端管理',
    meta: { title: '客户端管理', icon: 'example' },
    children: [
      {
        path: 'list',
        name: '客户端列表',
        component: () => import('@/views/common/list'),
        meta: { title: '客户端列表', icon: 'table' }
      },
      {
        path: 'add',
        name: '新增客户端',
        component: () => import('@/views/common/add'),
        meta: { title: '新增客户端', icon: 'tree' }
      },
      {
        path: 'edit',
        name: '编辑客户端',
        component: () => import('@/views/common/edit'),
        meta: { title: '编辑客户端', icon: 'tree' },
        hidden: true
      }
    ]
  },

  {
    path: '/sys/permission',
    component: Layout,
    redirect: '/sys/permission/list',
    name: '权限管理',
    meta: { title: '权限管理', icon: 'example' },
    children: [
      {
        path: 'list',
        name: '权限列表',
        component: () => import('@/views/common/list'),
        meta: { title: '权限列表', icon: 'table' }
      },
      {
        path: 'add',
        name: '新增权限',
        component: () => import('@/views/common/add'),
        meta: { title: '新增权限', icon: 'tree' }
      },
      {
        path: 'edit',
        name: '编辑权限',
        component: () => import('@/views/common/edit'),
        meta: { title: '编辑权限', icon: 'tree' },
        hidden: true
      }
    ]
  },

  {
    path: '/sys/role',
    component: Layout,
    redirect: '/sys/role/list',
    name: '等级管理',
    meta: { title: '等级管理', icon: 'example' },
    children: [
      {
        path: 'list',
        name: '等级列表',
        component: () => import('@/views/role/list'),
        meta: { title: '等级列表', icon: 'table' }
      },
      {
        path: 'add',
        name: '新增等级',
        component: () => import('@/views/role/add'),
        meta: { title: '新增等级', icon: 'tree' }
      },
      {
        path: 'edit',
        name: '编辑等级',
        component: () => import('@/views/role/edit'),
        meta: { title: '编辑等级', icon: 'tree' },
        hidden: true
      }
    ]
  },

  { path: '*', redirect: '/404', hidden: true }
]

export default new Router({
  // mode: 'history', //后端支持可开
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRouterMap
})

